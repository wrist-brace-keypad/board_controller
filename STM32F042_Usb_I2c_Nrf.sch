EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F0:STM32F042K6Tx U2
U 1 1 5ED9546C
P 5500 4150
F 0 "U2" H 5500 3061 50  0000 C CNN
F 1 "STM32F042K6Tx" H 5500 2970 50  0000 C CNN
F 2 "LQFP32Small:LQFP-32_7x7mm_Pitch0.8mm_small_pads" H 5100 3250 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00105814.pdf" H 5500 4150 50  0001 C CNN
	1    5500 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5ED95476
P 1700 950
F 0 "J1" H 1780 942 50  0000 L CNN
F 1 "Conn_01x02" H 1780 851 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1700 950 50  0001 C CNN
F 3 "~" H 1700 950 50  0001 C CNN
	1    1700 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5ED95494
P 5050 5250
F 0 "#PWR06" H 5050 5000 50  0001 C CNN
F 1 "GND" H 5055 5077 50  0000 C CNN
F 2 "" H 5050 5250 50  0001 C CNN
F 3 "" H 5050 5250 50  0001 C CNN
	1    5050 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J3
U 1 1 5ED9549E
P 4350 6850
F 0 "J3" H 4400 7167 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4400 7076 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 4350 6850 50  0001 C CNN
F 3 "~" H 4350 6850 50  0001 C CNN
	1    4350 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 6750 3250 6750
Wire Wire Line
	3250 6750 3250 6800
Wire Wire Line
	4150 6850 3600 6850
Wire Wire Line
	4150 6950 3600 6950
Wire Wire Line
	4150 7050 3600 7050
Wire Wire Line
	4650 6950 5300 6950
Wire Wire Line
	4650 6850 5300 6850
Wire Wire Line
	4650 6750 5300 6750
NoConn ~ 4650 7050
Text Label 3750 6850 0    50   ~ 0
nRF_Ce
Text Label 3750 6950 0    50   ~ 0
SCK
Text Label 3750 7050 0    50   ~ 0
MISO
Text Label 4750 6950 0    50   ~ 0
MOSI
Text Label 4750 6850 0    50   ~ 0
nRF_CSn
Text Label 4750 6750 0    50   ~ 0
VDD
$Comp
L power:GND #PWR04
U 1 1 5ED954B7
P 3250 6800
F 0 "#PWR04" H 3250 6550 50  0001 C CNN
F 1 "GND" H 3255 6627 50  0000 C CNN
F 2 "" H 3250 6800 50  0001 C CNN
F 3 "" H 3250 6800 50  0001 C CNN
	1    3250 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5ED954C1
P 1050 1000
F 0 "#PWR01" H 1050 750 50  0001 C CNN
F 1 "GND" H 1055 827 50  0000 C CNN
F 2 "" H 1050 1000 50  0001 C CNN
F 3 "" H 1050 1000 50  0001 C CNN
	1    1050 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 950  1050 950 
Wire Wire Line
	1050 950  1050 1000
Wire Wire Line
	1500 1050 1250 1050
Text Label 1300 1050 0    50   ~ 0
V_USB
Wire Wire Line
	5050 5250 5050 5200
Wire Wire Line
	5050 5200 5400 5200
Wire Wire Line
	5500 5200 5500 5150
Wire Wire Line
	5400 5150 5400 5200
Connection ~ 5400 5200
Wire Wire Line
	5400 5200 5500 5200
Wire Wire Line
	5400 3250 5400 3200
Wire Wire Line
	5400 3200 5500 3200
Wire Wire Line
	5500 3200 5500 3250
Wire Wire Line
	5500 3200 5600 3200
Wire Wire Line
	5600 3200 5600 3250
Connection ~ 5500 3200
Wire Wire Line
	5600 3200 5600 3050
Connection ~ 5600 3200
Text Label 5600 3050 0    50   ~ 0
VDD
Wire Wire Line
	6000 3950 6600 3950
Wire Wire Line
	6000 4050 6600 4050
Wire Wire Line
	6000 4150 6600 4150
Text Label 6300 3950 0    50   ~ 0
SCK
Text Label 6300 4050 0    50   ~ 0
MISO
Text Label 6300 4150 0    50   ~ 0
MOSI
Wire Wire Line
	6000 3850 6600 3850
Text Label 6300 3850 0    50   ~ 0
nRF_Ce
Text Label 4650 4250 0    50   ~ 0
nRF_CSn
Wire Wire Line
	5000 4250 4550 4250
$Comp
L Device:R R2
U 1 1 5ED9551F
P 4800 3200
F 0 "R2" H 4870 3246 50  0000 L CNN
F 1 "1k" H 4870 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4730 3200 50  0001 C CNN
F 3 "~" H 4800 3200 50  0001 C CNN
	1    4800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3050 4800 3000
Wire Wire Line
	4800 3000 5500 3000
Wire Wire Line
	5500 3000 5500 3050
Wire Wire Line
	5500 3050 5600 3050
Connection ~ 5600 3050
Wire Wire Line
	5000 3450 4800 3450
Wire Wire Line
	4800 3450 4800 3350
Text Label 6250 4750 0    50   ~ 0
SWD_IO
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 5ED95531
P 7650 4750
F 0 "J5" H 7730 4792 50  0000 L CNN
F 1 "Conn_01x03" H 7730 4701 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 7650 4750 50  0001 C CNN
F 3 "~" H 7650 4750 50  0001 C CNN
	1    7650 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5ED9553B
P 7300 4500
F 0 "#PWR08" H 7300 4250 50  0001 C CNN
F 1 "GND" H 7305 4327 50  0000 C CNN
F 2 "" H 7300 4500 50  0001 C CNN
F 3 "" H 7300 4500 50  0001 C CNN
	1    7300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4750 6600 4750
Wire Wire Line
	6000 4850 6600 4850
Wire Wire Line
	7450 4650 7450 4450
Wire Wire Line
	7450 4450 7300 4450
Wire Wire Line
	7300 4450 7300 4500
$Comp
L Device:C C3
U 1 1 5ED9554A
P 5200 2550
F 0 "C3" H 5315 2596 50  0000 L CNN
F 1 "0.1uF" H 5315 2505 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5238 2400 50  0001 C CNN
F 3 "~" H 5200 2550 50  0001 C CNN
	1    5200 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5ED95554
P 5400 2700
F 0 "#PWR07" H 5400 2450 50  0001 C CNN
F 1 "GND" H 5405 2527 50  0000 C CNN
F 2 "" H 5400 2700 50  0001 C CNN
F 3 "" H 5400 2700 50  0001 C CNN
	1    5400 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2700 5200 2700
Wire Wire Line
	5200 2400 5200 2300
Wire Wire Line
	5200 2300 4950 2300
Wire Wire Line
	5600 3050 5850 3050
$Comp
L Mechanical:MountingHole H1
U 1 1 5ED95563
P 7900 1750
F 0 "H1" H 8000 1796 50  0000 L CNN
F 1 "MountingHole" H 8000 1705 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 7900 1750 50  0001 C CNN
F 3 "~" H 7900 1750 50  0001 C CNN
	1    7900 1750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5ED9556D
P 8650 1750
F 0 "H2" H 8750 1796 50  0000 L CNN
F 1 "MountingHole" H 8750 1705 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 8650 1750 50  0001 C CNN
F 3 "~" H 8650 1750 50  0001 C CNN
	1    8650 1750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5ED95577
P 9400 1750
F 0 "H3" H 9500 1796 50  0000 L CNN
F 1 "MountingHole" H 9500 1705 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 9400 1750 50  0001 C CNN
F 3 "~" H 9400 1750 50  0001 C CNN
	1    9400 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4750 4650 4750
Wire Wire Line
	5000 4850 4650 4850
$Comp
L Device:LED D1
U 1 1 5ED955A1
P 6750 3250
F 0 "D1" H 6743 3466 50  0000 C CNN
F 1 "LED" H 6743 3375 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 6750 3250 50  0001 C CNN
F 3 "~" H 6750 3250 50  0001 C CNN
	1    6750 3250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5ED955AB
P 7100 3250
F 0 "R1" H 7170 3296 50  0000 L CNN
F 1 "1k" H 7170 3205 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7030 3250 50  0001 C CNN
F 3 "~" H 7100 3250 50  0001 C CNN
	1    7100 3250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5ED955B5
P 7550 3250
F 0 "#PWR05" H 7550 3000 50  0001 C CNN
F 1 "GND" H 7555 3077 50  0000 C CNN
F 2 "" H 7550 3250 50  0001 C CNN
F 3 "" H 7550 3250 50  0001 C CNN
	1    7550 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3550 6000 3550
Wire Wire Line
	6000 4550 6600 4550
Wire Wire Line
	6000 4650 6600 4650
Wire Wire Line
	6000 3650 6600 3650
Wire Wire Line
	6000 3750 6600 3750
Text Label 4300 4850 0    50   ~ 0
I2C1_SDA
Text Label 4300 4750 0    50   ~ 0
I2C1_SCL
Text Label 6250 3750 0    50   ~ 0
U2_RX
Text Label 6250 3650 0    50   ~ 0
U2_TX
$Comp
L Mechanical:MountingHole H4
U 1 1 5ED955EC
P 10300 1750
F 0 "H4" H 10400 1796 50  0000 L CNN
F 1 "MountingHole" H 10400 1705 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 10300 1750 50  0001 C CNN
F 3 "~" H 10300 1750 50  0001 C CNN
	1    10300 1750
	1    0    0    -1  
$EndComp
Text Label 6250 4850 0    50   ~ 0
SWD_CLK
Text Label 7100 4750 0    50   ~ 0
SWD_CLK
Text Label 7100 4850 0    50   ~ 0
SWD_IO
Wire Wire Line
	7100 4850 7450 4850
Wire Wire Line
	7100 4750 7450 4750
Text Label 4800 3450 0    50   ~ 0
nRST
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5ED955FC
P 4400 3450
F 0 "J4" H 4318 3225 50  0000 C CNN
F 1 "Conn_01x01" H 4318 3316 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 4400 3450 50  0001 C CNN
F 3 "~" H 4400 3450 50  0001 C CNN
	1    4400 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4800 3450 4600 3450
Connection ~ 4800 3450
$Comp
L Regulator_Linear:AP1117-33 U1
U 1 1 5EDC0A29
P 1650 1800
F 0 "U1" H 1650 2042 50  0000 C CNN
F 1 "AP1117-33" H 1650 1951 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 1650 2000 50  0001 C CNN
F 3 "http://www.diodes.com/datasheets/AP1117.pdf" H 1750 1550 50  0001 C CNN
	1    1650 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5EDC0A33
P 1650 2250
F 0 "#PWR03" H 1650 2000 50  0001 C CNN
F 1 "GND" H 1655 2077 50  0000 C CNN
F 2 "" H 1650 2250 50  0001 C CNN
F 3 "" H 1650 2250 50  0001 C CNN
	1    1650 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EDC0A3D
P 1100 2100
F 0 "C1" H 1215 2146 50  0000 L CNN
F 1 "10uF" H 1215 2055 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1138 1950 50  0001 C CNN
F 3 "~" H 1100 2100 50  0001 C CNN
	1    1100 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5EDC0A47
P 2150 2050
F 0 "C2" H 2265 2096 50  0000 L CNN
F 1 "1uF" H 2265 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2188 1900 50  0001 C CNN
F 3 "~" H 2150 2050 50  0001 C CNN
	1    2150 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1800 1100 1800
Wire Wire Line
	1100 1800 1100 1950
Wire Wire Line
	1100 1800 850  1800
Connection ~ 1100 1800
Wire Wire Line
	1100 2250 1650 2250
Wire Wire Line
	1650 2100 1650 2250
Connection ~ 1650 2250
Wire Wire Line
	2150 2200 2150 2250
Wire Wire Line
	2150 2250 1650 2250
Wire Wire Line
	2150 1900 2150 1800
Wire Wire Line
	2150 1800 1950 1800
Connection ~ 2150 1800
Text Label 2350 1800 0    50   ~ 0
VDD
Text Label 900  1800 0    50   ~ 0
V_USB
$Comp
L Connector:USB_B_Mini J6
U 1 1 5EDD2D99
P 9550 4450
F 0 "J6" H 9607 4917 50  0000 C CNN
F 1 "USB_B_Mini" H 9607 4826 50  0000 C CNN
F 2 "UsbMiniBLargerHoles:USB_Mini-B_Lrg_Holes" H 9700 4400 50  0001 C CNN
F 3 "~" H 9700 4400 50  0001 C CNN
	1    9550 4450
	1    0    0    -1  
$EndComp
NoConn ~ 9450 4850
NoConn ~ 9850 4650
Wire Wire Line
	9550 4850 9550 5050
Wire Wire Line
	9550 5050 9650 5050
Wire Wire Line
	9850 4550 10250 4550
Wire Wire Line
	9850 4450 10250 4450
Wire Wire Line
	9850 4250 10250 4250
Text Label 10000 4250 0    50   ~ 0
V_USB
Text Label 10050 4450 0    50   ~ 0
D_P
Text Label 10050 4550 0    50   ~ 0
D_N
Text Label 6250 4550 0    50   ~ 0
D_N
Text Label 6250 4650 0    50   ~ 0
D_P
NoConn ~ 6000 4250
NoConn ~ 6000 4350
NoConn ~ 6000 4450
NoConn ~ 6000 4950
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5EDF39E5
P 2350 4700
F 0 "J2" H 2430 4692 50  0000 L CNN
F 1 "Conn_01x04" H 2430 4601 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 2350 4700 50  0001 C CNN
F 3 "~" H 2350 4700 50  0001 C CNN
	1    2350 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4600 1450 4600
Wire Wire Line
	1450 4600 1450 4700
Wire Wire Line
	1450 4200 950  4200
Wire Wire Line
	2150 4800 1650 4800
Wire Wire Line
	2150 4900 1650 4900
$Comp
L power:GND #PWR02
U 1 1 5EDFDEF6
P 1450 4700
F 0 "#PWR02" H 1450 4450 50  0001 C CNN
F 1 "GND" H 1455 4527 50  0000 C CNN
F 2 "" H 1450 4700 50  0001 C CNN
F 3 "" H 1450 4700 50  0001 C CNN
	1    1450 4700
	1    0    0    -1  
$EndComp
Text Label 1100 4200 0    50   ~ 0
VDD
Text Label 1750 4800 0    50   ~ 0
I2C1_SCL
Text Label 1750 4900 0    50   ~ 0
I2C1_SDA
NoConn ~ 5000 3950
NoConn ~ 5000 4050
NoConn ~ 5000 4350
NoConn ~ 5000 4450
NoConn ~ 5000 4550
NoConn ~ 8150 -200
$Comp
L power:GND #PWR0101
U 1 1 5EE29DC5
P 9650 5050
F 0 "#PWR0101" H 9650 4800 50  0001 C CNN
F 1 "GND" H 9655 4877 50  0000 C CNN
F 2 "" H 9650 5050 50  0001 C CNN
F 3 "" H 9650 5050 50  0001 C CNN
	1    9650 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 5EE7598F
P 7600 3700
F 0 "J7" H 7680 3742 50  0000 L CNN
F 1 "Conn_01x03" H 7680 3651 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 7600 3700 50  0001 C CNN
F 3 "~" H 7600 3700 50  0001 C CNN
	1    7600 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5EE75999
P 7250 3450
F 0 "#PWR09" H 7250 3200 50  0001 C CNN
F 1 "GND" H 7255 3277 50  0000 C CNN
F 2 "" H 7250 3450 50  0001 C CNN
F 3 "" H 7250 3450 50  0001 C CNN
	1    7250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3600 7400 3400
Wire Wire Line
	7400 3400 7250 3400
Wire Wire Line
	7250 3400 7250 3450
Wire Wire Line
	7050 3800 7400 3800
Wire Wire Line
	7050 3700 7400 3700
Text Label 7100 3700 0    50   ~ 0
U2_TX
Text Label 7100 3800 0    50   ~ 0
U2_RX
$Comp
L Device:C C4
U 1 1 5EE8D868
P 1450 4400
F 0 "C4" H 1565 4446 50  0000 L CNN
F 1 "1uF" H 1565 4355 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1488 4250 50  0001 C CNN
F 3 "~" H 1450 4400 50  0001 C CNN
	1    1450 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 4200 1450 4250
Wire Wire Line
	1450 4550 1450 4600
Connection ~ 1450 4600
Wire Wire Line
	2150 4700 1650 4700
Text Label 1800 4700 0    50   ~ 0
VDD
Wire Wire Line
	6600 3550 6600 3250
Wire Wire Line
	6900 3250 6950 3250
Wire Wire Line
	7250 3250 7550 3250
$Comp
L Device:R R3
U 1 1 5ED99A50
P 4800 4650
F 0 "R3" H 4870 4696 50  0000 L CNN
F 1 "1k" H 4870 4605 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4730 4650 50  0001 C CNN
F 3 "~" H 4800 4650 50  0001 C CNN
	1    4800 4650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5ED9A194
P 4800 4950
F 0 "R4" H 4870 4996 50  0000 L CNN
F 1 "1k" H 4870 4905 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4730 4950 50  0001 C CNN
F 3 "~" H 4800 4950 50  0001 C CNN
	1    4800 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 4950 4950 4950
Wire Wire Line
	4650 4950 4650 4850
Connection ~ 4650 4850
Wire Wire Line
	4650 4650 4650 4750
Connection ~ 4650 4750
Wire Wire Line
	4950 4650 5000 4650
Wire Wire Line
	4300 4750 4650 4750
Wire Wire Line
	4300 4850 4650 4850
$Comp
L Device:R R5
U 1 1 5EDC391C
P 3600 1900
F 0 "R5" H 3670 1946 50  0000 L CNN
F 1 "10k" H 3670 1855 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3530 1900 50  0001 C CNN
F 3 "~" H 3600 1900 50  0001 C CNN
	1    3600 1900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5EDC483D
P 3600 2250
F 0 "R6" H 3670 2296 50  0000 L CNN
F 1 "10k" H 3670 2205 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3530 2250 50  0001 C CNN
F 3 "~" H 3600 2250 50  0001 C CNN
	1    3600 2250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 5EDC4D54
P 3950 2100
F 0 "J8" H 3868 1875 50  0000 C CNN
F 1 "Conn_01x01" H 3868 1966 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 3950 2100 50  0001 C CNN
F 3 "~" H 3950 2100 50  0001 C CNN
	1    3950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2050 3600 2100
Wire Wire Line
	3750 2100 3600 2100
Connection ~ 3600 2100
Wire Wire Line
	3600 2400 3600 2450
Wire Wire Line
	3600 2450 3350 2450
Wire Wire Line
	2150 1800 2650 1800
$Comp
L power:GND #PWR0102
U 1 1 5EDE1131
P 3350 2450
F 0 "#PWR0102" H 3350 2200 50  0001 C CNN
F 1 "GND" H 3355 2277 50  0000 C CNN
F 2 "" H 3350 2450 50  0001 C CNN
F 3 "" H 3350 2450 50  0001 C CNN
	1    3350 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 1750 3600 1700
Wire Wire Line
	3600 1700 3450 1700
Text Label 3450 1700 0    50   ~ 0
V_USB
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 5EDED2E8
P 6300 3250
F 0 "J9" H 6218 3025 50  0000 C CNN
F 1 "Conn_01x01" H 6218 3116 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01_Pitch2.54mm" H 6300 3250 50  0001 C CNN
F 3 "~" H 6300 3250 50  0001 C CNN
	1    6300 3250
	1    0    0    -1  
$EndComp
Text Label 5050 2300 0    50   ~ 0
VDD
Wire Wire Line
	6100 3450 6100 3250
Wire Wire Line
	6000 3450 6100 3450
Text Label 6100 3350 0    50   ~ 0
A0
$EndSCHEMATC
